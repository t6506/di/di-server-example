package org.example.app.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.app.domain.Apartment;
import org.example.app.dto.ApartmentRQ;
import org.example.app.dto.ApartmentRS;
import org.example.app.exception.ItemNotFoundException;
import org.example.framework.security.auth.exception.UserNotAuthorizedException;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.auth.annotation.Audit;
import org.example.framework.security.auth.annotation.HasRole;
import org.example.framework.server.auth.Roles;
import org.example.framework.server.auth.SecurityContext;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Component
@Slf4j
public class ApartmentManager {
    private final AtomicLong nextId = new AtomicLong(1);
    private final Map<Long, Apartment> items = new HashMap<>();

    @Audit
    @HasRole(Roles.USER)
    public ApartmentRS create(final ApartmentRQ createRQ) throws UserNotAuthorizedException {
        final Apartment item = new Apartment(
                nextId.getAndIncrement(),
                SecurityContext.getPrincipal().getName(),
                Instant.now().getEpochSecond(),
                createRQ.getNumberOfRooms(),
                createRQ.getPrice(),
                createRQ.getArea(),
                createRQ.isBalcony(),
                createRQ.isLoggia(),
                createRQ.getFloor(),
                createRQ.getFloorsInHouse()
        );
        synchronized (this) {
            log.debug("create item: {}", item);
            items.put(item.getId(), item);
            return new ApartmentRS(item);
        }
    }
    @Audit
    public synchronized ApartmentRS getById(final long id) throws ItemNotFoundException{
        final ApartmentRS item = Optional.ofNullable(items.get(id))
                .map(ApartmentRS::new)
                .orElseThrow(()-> new ItemNotFoundException(id));
        log.debug("return item: {}", item);
        return item;
    }

    @Audit
    public synchronized List<ApartmentRS> getAll() {
        return items.values().stream()
                .map(ApartmentRS::new)
                .collect(Collectors.toList());
    }
    @Audit
    @HasRole(Roles.USER)
    public ApartmentRS removeById(final long id) throws ItemNotFoundException, UserNotAuthorizedException {
        synchronized (this) {
            assertContainsAndOwner(id);
            final ApartmentRS removed = new ApartmentRS(items.remove(id));
            log.debug("remove item by id {} success: {}", id, removed);
            return removed;
        }
    }
    @Audit
    @HasRole(Roles.USER)
    public ApartmentRS update(final ApartmentRQ updateRQ, final long id)
            throws ItemNotFoundException, UserNotAuthorizedException {
        synchronized (this) {
            assertContainsAndOwner(id);
            final Apartment item = items.get(id);
            final Apartment updatedItem = new Apartment(
                    id,
                    item.getOwner(),
                    item.getCreated(),
                    updateRQ.getNumberOfRooms(),
                    updateRQ.getPrice(),
                    updateRQ.getArea(),
                    updateRQ.isBalcony(),
                    updateRQ.isLoggia(),
                    updateRQ.getFloor(),
                    updateRQ.getFloorsInHouse()
            );
            items.put(id, updatedItem);
            log.debug("update item id {}: {}", id, updatedItem);
            return new ApartmentRS(updatedItem);
        }
    }

    private void assertContainsAndOwner(final long id) throws ItemNotFoundException, UserNotAuthorizedException {
        if (!items.containsKey(id)) {
            throw new ItemNotFoundException(id);
        }
        if (!items.get(id).getOwner().equals(SecurityContext.getPrincipal().getName())) {
            throw new UserNotAuthorizedException(SecurityContext.getPrincipal().getName());
        }
    }
}
