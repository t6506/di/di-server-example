package org.example.app.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.dto.ApartmentRQ;
import org.example.app.dto.ApartmentRS;
import org.example.app.exception.ItemNotFoundException;
import org.example.framework.security.auth.exception.UserNotAuthenticatedException;
import org.example.framework.security.auth.exception.UserNotAuthorizedException;
import org.example.app.manager.ApartmentManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.*;
import org.example.framework.server.http.HttpMethod;

import java.util.List;

@Slf4j
@Controller
@RequiredArgsConstructor
@Component
public class ApartmentController {
    private final ApartmentManager manager;

    @RequestMapping(method = HttpMethod.GET, path = "^/apartments$")
    @ResponseBody
    public List<ApartmentRS> getAll() {
        return manager.getAll();
    }

    @RequestMapping(method = HttpMethod.GET, path = "^/apartments/(?<apartmentId>[0-9]{1,9})$")
    @ResponseBody
    public ApartmentRS getById(@Pathvariable("apartmentId") final long id) throws ItemNotFoundException {
        return manager.getById(id);
    }

    @RequestMapping(method = HttpMethod.POST, path = "^/apartments$")
    @ResponseBody
    public ApartmentRS create(@RequestBody final ApartmentRQ createRQ) {
        try {
            return manager.create(createRQ);
        } catch (UserNotAuthorizedException | UserNotAuthenticatedException e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = HttpMethod.POST, path = "^/apartments/(?<apartmentId>[0-9]{1,9})/update$")
    @ResponseBody
    public ApartmentRS update(@RequestBody final ApartmentRQ createRQ, @Pathvariable("apartmentId") final long id) {
        try {
            return manager.update(createRQ, id);
        } catch (UserNotAuthorizedException | UserNotAuthenticatedException | ItemNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = HttpMethod.POST, path = "^/apartments/(?<apartmentId>[0-9]{1,9})/delete$")
    @ResponseBody
    public ApartmentRS delete(@Pathvariable("apartmentId") final long id) {
        try {
            return manager.removeById(id);
        } catch (UserNotAuthorizedException | UserNotAuthenticatedException | ItemNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
