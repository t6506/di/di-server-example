package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.manager.UserManager;
import org.example.framework.di.Container;
import org.example.framework.security.auth.processor.AuditBeanPostProcessor;
import org.example.framework.security.auth.processor.HasRoleBeanPostProcessor;
import org.example.framework.server.annotation.Controller;
import org.example.framework.server.controller.ControllerRegistrar;
import org.example.framework.server.controller.method.handler.ReturnValueHandler;
import org.example.framework.server.http.Server;
import org.example.framework.security.middleware.AnonAuthMiddleware;
import org.example.framework.security.middleware.jsonbody.JsonBodyAuthMiddleware;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.util.Lists;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Slf4j
public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        String password = new String(Files.readAllBytes(Paths.get("password.txt")));
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", password);

        final Container container = new Container(Lists.of(new HasRoleBeanPostProcessor(), new AuditBeanPostProcessor()));
        container.register(Gson.class);
        container.register(Argon2PasswordEncoder.class);
        container.register("org.example");
        container.wire();

        final Gson gson = container.getBean(Gson.class);
        final UserManager userManager = container.getBean(UserManager.class);


        final Server server = Server.builder()
                .middleware(new JsonBodyAuthMiddleware(userManager, gson))
                .middleware(new AnonAuthMiddleware())
                .argumentResolvers(container.getBeansByType(ArgumentResolver.class))
                .returnValueHandlers(container.getBeansByType(ReturnValueHandler.class))
                .router(new ControllerRegistrar().register(container.getBeansByAnnotation(Controller.class)))
                .build();

        final int port = 8443;

        server.start(port);

        Thread.sleep(100_000);

        server.stop();
    }
}
