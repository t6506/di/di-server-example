package org.example.app.exception;

public class LoginAlreadyRegisteredException extends Exception {
    public LoginAlreadyRegisteredException(String userName) {
        super("user " + userName + " already registered");
    }

}
