package org.example.app.method.handler;

import lombok.RequiredArgsConstructor;
import org.example.framework.di.annotation.Autowired;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.ResponseBody;
import org.example.framework.server.controller.method.converter.HttpMessageConverter;
import org.example.framework.server.controller.method.handler.ReturnValueHandler;
import org.example.framework.server.exception.NoSupportingMessageConverter;
import org.example.framework.server.exception.UnsupportedReturnTypeException;
import org.example.framework.server.http.MediaType;
import org.example.framework.server.http.Request;
import org.example.framework.server.http.Response;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Autowired
@Component
public class ResponseBodyReturnValueHandler implements ReturnValueHandler {

    private final List<HttpMessageConverter> messageConverters = new ArrayList<>();

    @Autowired
    public void setMessageConverters(final List<HttpMessageConverter> messageConverters) {
        this.messageConverters.addAll(messageConverters);
    }


    @Override
    public boolean supportReturnType(final Method method) {
        return method.isAnnotationPresent(ResponseBody.class);
    }

    @Override
    public void handleReturnType(final Object returnValue, final Method method, final Request request, final Response response) throws Exception {
        if (!supportReturnType(method)) {
            throw new UnsupportedReturnTypeException();
        }
        final Class<?> returnType = method.getReturnType();

        final MediaType mediaType = request.getAccept();
        for (HttpMessageConverter messageConverter : messageConverters) {
            if (messageConverter.canWrite(returnType, mediaType)) {
                messageConverter.write(returnValue, mediaType, response);
                return;
            }
        }
        throw new NoSupportingMessageConverter(mediaType.value());
    }
}
