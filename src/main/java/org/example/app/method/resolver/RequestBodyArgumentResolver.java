package org.example.app.method.resolver;

import lombok.RequiredArgsConstructor;
import org.example.framework.di.annotation.Autowired;
import org.example.framework.di.annotation.Component;
import org.example.framework.server.annotation.RequestBody;
import org.example.framework.server.controller.method.converter.HttpMessageConverter;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.exception.NoSupportingMessageConverter;
import org.example.framework.server.exception.UnsupportedParameterException;
import org.example.framework.server.http.MediaType;
import org.example.framework.server.http.Request;

import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Autowired
@Component
public class RequestBodyArgumentResolver implements ArgumentResolver {
    private final List<HttpMessageConverter> messageConverters = new ArrayList<>();

    @Autowired
    public void setMessageConverters(final List<HttpMessageConverter> messageConverters) {
        this.messageConverters.addAll(messageConverters);
    }

    @Override
    public boolean supportParameter(final Parameter parameter) {
        return parameter.isAnnotationPresent(RequestBody.class);
    }

    @Override
    public Object resolveArgument(final Parameter parameter, final Request request) throws Exception {
        if (!supportParameter(parameter)) {
            throw new UnsupportedParameterException(parameter.getName());
        }
        final Class<?> paramClazz = parameter.getType();

        final MediaType mediaType = request.getContentType();
        for (HttpMessageConverter messageConverter : messageConverters) {
            if (messageConverter.canRead(paramClazz, mediaType)) {
                return messageConverter.read(paramClazz, request);
            }

        }
        throw new NoSupportingMessageConverter(mediaType.value());
    }
}
